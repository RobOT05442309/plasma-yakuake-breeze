# Plasma Yakuake Breeze

This repo is a theme for Yakuake based on original Breeze theme, this theme match with the "light" version of Breeze (original Breeze theme is dark).

## Instructions

Download this repo and copy the content of it in the following folder (user directory):

~/.local/share/yakuake/skins/breeze-light

Restart Yakuake, you should be able to select "Breeze Light" theme.

## Links

This repo is available from this link :

- https://gitlab.com/RobOT05442309/plasma-yakuake-breeze (Original)
- https://github.com/RobOT05442309/plasma-yakuake-breeze (Mirror)

If you want to report something, please use the Gitlab repository.